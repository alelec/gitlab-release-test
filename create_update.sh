#!/bin/bash

echo "Existing releases"
git tag

echo ""
echo "Next release:"
next_vers=$(python __version__.py --next --short)
echo ${next_vers} | tee version.txt

echo "Create new commit, create tag, push to repo"
git add version.txt
git commit -m "Release ${next_vers}"
git tag ${next_vers}
git push --all
git push --tags
sleep 2
